package com.typeform

import com.typeform.pages.LoginPage
import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import com.typeform.utils.CaptchaUtils

class LoginTest: TestBase () {

    private val loginPage by lazy { LoginPage(driver) }

    @Test(priority=0)
    fun `it should display the login field`() {
        loginPage.dismissCookies()
        assertTrue(loginPage.isEmailFieldDisplayed())
        assertTrue(loginPage.isPasswordFieldDisplayed())
        assertTrue(loginPage.isLoginButtonDisplayed())
    }

    @Test(priority=1)
    fun `introducing a wrong password displays an error` () {
        loginPage
                .setMail()
                .setPassword("incorrectPassword")
                .clickLogin()

        CaptchaUtils().bypassCaptcha()

        assertTrue(loginPage.isWrongPasswordMessageDisplayed())
    }

    @Test
    fun `introducing correct credentials navigates to captcha`() {
        loginPage
                .setMail()
                .setPassword()
                .clickLogin()

        CaptchaUtils().bypassCaptcha()
    }
}