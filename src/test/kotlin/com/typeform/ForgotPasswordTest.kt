package com.typeform

import com.typeform.pages.ForgotPasswordPage
import com.typeform.pages.LoginPage
import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import com.typeform.utils.CaptchaUtils

class ForgotPasswordTest: TestBase () {

    private val loginPage by lazy { LoginPage(driver) }
    lateinit var forgotPasswordPage: ForgotPasswordPage

    @Test(priority=0)
    fun `it should display the login field`() {
        loginPage.dismissCookies()
        assertTrue(loginPage.isEmailFieldDisplayed())
        assertTrue(loginPage.isPasswordFieldDisplayed())
        assertTrue(loginPage.isLoginButtonDisplayed())
    }

    @Test(priority=1)
    fun `setting a mail and clicking on forgot password navigates to retrieve password flow`() {
        forgotPasswordPage = loginPage
                .setMail(MAIL)
                .clickForgotPassword()

        assertTrue(forgotPasswordPage.isEmailFieldDisplayed())
        assertTrue(forgotPasswordPage.emailDefaultValueIs(MAIL))
        assertTrue(forgotPasswordPage.isFormDisplayed())
    }

    @Test(priority=2)
    fun `clicking submit button with a mail set displays a mail sent modal `() {
        forgotPasswordPage.clickSubmit()

        CaptchaUtils().bypassCaptcha()

        assertTrue(forgotPasswordPage.isModalDisplayed())
    }


    companion object {
        const val MAIL = "b.gabriel.valera@gmail.com"
    }
}