package com.typeform

import com.typeform.pages.LoginPage
import com.typeform.pages.RegisterPage
import com.typeform.utils.CaptchaUtils
import com.typeform.utils.MailUtils
import org.testng.annotations.Test
import kotlin.test.assertTrue

class RegisterTest: TestBase () {

    lateinit var registerPage: RegisterPage

    @Test(priority = 0)
    fun `clicking on register navigates to the registration flow`() {
        val loginPage = LoginPage(driver)
        loginPage.dismissCookies()

        registerPage = loginPage.clickRegister()
        assertTrue(registerPage.isEmailFieldDisplayed())
        assertTrue(registerPage.isPasswordFieldDisplayed())
    }

    @Test(priority = 1)
    fun `entering a valid mail and passwords starts the registration process `() {
        val user = MailUtils().generateRandomUser()
        registerPage
                .setMail(user.mail)
                .setPassword(user.password)
                .clickCreateAccount()

        CaptchaUtils().bypassCaptcha()

        registerPage
                .clickCreateAccount() // second time to confirm options
    }

}