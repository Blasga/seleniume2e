package com.typeform


import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.testng.annotations.AfterTest
import org.testng.annotations.BeforeTest
import java.util.concurrent.TimeUnit

abstract class TestBase {
    private val baseUrl: String = "https://admin.typeform.com/login/"

    lateinit var driver: WebDriver

    @BeforeTest
    fun setup() {
        driver = ChromeDriver()

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)
        driver.manage().window().maximize()

        driver.get(baseUrl)
    }

    @AfterTest
    fun driverClose() = driver.close()
}