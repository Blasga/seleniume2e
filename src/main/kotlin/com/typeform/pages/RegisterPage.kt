package com.typeform.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

class RegisterPage(driver: WebDriver): BasePage(driver) {

    @FindBy(css = "[data-qa=$EMAIL_FIELD]")
    lateinit var emailField: WebElement

    @FindBy(css = "[data-qa=$PASSWORD_FIELD]")
    lateinit var passwordField: WebElement

    @FindBy(css = "[data-qa=$SUBMIT]")
    lateinit var submitButton: WebElement

    @FindBy(id = "terms")
    lateinit var termsCheckbox: WebElement

    @FindBy(id = "consents")
    lateinit var consentsCheckbox: WebElement

    fun isEmailFieldDisplayed() = waitForElementDisplayed(emailField)

    fun isPasswordFieldDisplayed() = waitForElementDisplayed(passwordField)

    fun toggleTermsCheckbox() = waitAndClick(termsCheckbox)

    fun toggleConsentsCheckbox() = waitAndClick(consentsCheckbox)

    fun clickCreateAccount() = waitAndClick(submitButton)

    fun setMail(mail: String = "b.gabriel.valera@gmail.com") = also { emailField.sendKeys(mail) }

    fun setPassword(password: String  = "automationAssignment") =  also {  passwordField.sendKeys(password) }


    companion object {
        const val EMAIL_FIELD = "field-email"
        const val PASSWORD_FIELD = "field-password"
        const val SUBMIT = "button-submit"
    }
}