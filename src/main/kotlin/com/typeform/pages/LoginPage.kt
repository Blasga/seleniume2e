package com.typeform.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

class LoginPage(driver: WebDriver): BasePage(driver) {

    @FindBy(css = "[data-qa=$EMAIL_FIELD]")
    lateinit var emailField: WebElement

    @FindBy(css = "[data-qa=$PASSWORD_FIELD]")
    lateinit var passwordField: WebElement

    @FindBy(css = "[data-qa=$FORGOT_PASSWORD]")
    lateinit var forgotPassword: WebElement

    @FindBy(id = "btnlogin")
    lateinit var loginButton: WebElement

    @FindBy(id = "error")
    lateinit var passwordErrorMessage: WebElement

    @FindBy(linkText = "Sign up")
    lateinit var registerButton: WebElement

    fun isEmailFieldDisplayed() = waitForElementDisplayed(emailField)

    fun isPasswordFieldDisplayed() = waitForElementDisplayed(passwordField)

    fun isLoginButtonDisplayed() = waitForElementDisplayed(loginButton)

    fun isWrongPasswordMessageDisplayed() = waitForElementDisplayed(passwordErrorMessage)

    fun setMail(mail: String = "b.gabriel.valera@gmail.com") = also { emailField.sendKeys(mail) }

    fun setPassword(password: String  = "automationAssignment") =  also {  passwordField.sendKeys(password) }

    fun clickLogin() = also{ waitAndClick(loginButton) }

    fun clickForgotPassword(): ForgotPasswordPage {
        waitAndClick(forgotPassword)
        return ForgotPasswordPage(driver)
    }

    fun clickRegister(): RegisterPage {
        waitAndClick(registerButton)
        return RegisterPage(driver)
    }

    companion object {
        const val EMAIL_FIELD = "field-email"
        const val PASSWORD_FIELD = "field-password"
        const val FORGOT_PASSWORD = "button-forgot-password"
    }
}