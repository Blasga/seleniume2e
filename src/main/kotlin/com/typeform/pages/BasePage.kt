package com.typeform.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

open class BasePage(val driver: WebDriver) {
    init {
        PageFactory.initElements(driver, this)
    }

    @FindBy(xpath = "//*[contains(text(), 'Accept All')]")
    lateinit var cookieAcceptButton: WebElement

    private fun waitForElementVisibility(element: WebElement, timeout: Long = 30) {
        WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element))
    }

    fun waitAndClick(element: WebElement) {
        waitForElementVisibility(element)
        element.click()
    }

    fun waitForElementDisplayed(element: WebElement): Boolean {
        waitForElementVisibility(element)
        return element.isDisplayed
    }

    fun dismissCookies() = waitAndClick(cookieAcceptButton)

}