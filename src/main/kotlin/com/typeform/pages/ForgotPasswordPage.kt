package com.typeform.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

class ForgotPasswordPage(driver: WebDriver): BasePage(driver) {
    init {

    }

    @FindBy(css = "[data-qa=$EMAIL_FIELD]")
    lateinit var emailField: WebElement


    @FindBy(css = "[data-qa=$FORGOT_PASSWORD_FORM]")
    lateinit var forgotPasswordForm: WebElement

    @FindBy(css = "[data-qa=$SUBMIT]")
    lateinit var submitButton: WebElement

    @FindBy(css = "[data-qa=$MODAL]")
    lateinit var modal: WebElement



    fun isEmailFieldDisplayed() = waitForElementDisplayed(emailField)

    fun isFormDisplayed() = waitForElementDisplayed(forgotPasswordForm)

    fun emailDefaultValueIs(mail: String) = emailField.text == mail

    fun isModalDisplayed() = waitForElementDisplayed(modal)

    fun clickSubmit() = waitAndClick(submitButton)

    companion object {
        const val EMAIL_FIELD = "field-email-password-reset"
        const val FORGOT_PASSWORD_FORM = "password-reset-request-form"
        const val SUBMIT = "button-submit-password-reset"
        const val MODAL = "modal"
    }
}